# Copyright 2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ user=accounts-sso tag=VERSION_${PV} suffix=tar.bz2 ] \
    meson \
    test-dbus-daemon

export_exlib_phases src_prepare src_test

SUMMARY="Accounts management library for GLib applications"
DESCRIPTION="
This project is a library for managing accounts which can be used from GLib
applications. It is part of the accounts-sso project.
"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    gtk-doc
    python
"

DEPENDENCIES="
    build:
        app-doc/gtk-doc-autotools[>=1.14] [[ note = [ for gtkdocize ] ]]
        app-text/docbook-xml-dtd:4.3 [[ note = [ for man pages ] ]]
        app-text/docbook-xsl-stylesheets [[ note = [ for man pages ] ]]
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        dev-db/sqlite:3[>=3.7.0]
        dev-libs/glib:2[>=2.35.1]
        dev-libs/libxml2:2.0
        gnome-desktop/gobject-introspection:1[>=1.30.0]
        python? ( gnome-bindings/pygobject:3[>=2.90] )
    test:
        dev-libs/check[>=0.9.4]
"

MESON_SOURCE=${WORKBASE}/${PN}-VERSION_${PV}-8948717702424ce15f4e23e5db2c8ce0799ec120

libaccounts-glib_src_prepare() {
    meson_src_prepare

    ! option gtk-doc && edo sed \
        -e '/docs/d' \
        -i meson.build

    ! option python && edo sed \
        -e '/pygobject/d' \
        -i ${PN}/meson.build

    ! expecting_tests && edo sed \
        -e '/tests/d' \
        -i meson.build

    # fix filesdir locations in the pkg-config file
    edo sed \
        -e "/filesdir/s:=\${prefix}:=/usr:" \
        -i ${PN}/${PN}.pc.in

}

libaccounts-glib_src_test() {
    test-dbus-daemon_start
    meson_src_test
    test-dbus-daemon_stop
}

