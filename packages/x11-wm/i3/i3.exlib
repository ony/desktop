# Copyright 2011-2012 Quentin "Sardem FF7" Glidic <sardemff7+exherbo@sardemff7.net>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'i3-4.15' from Void Linux, which is:
#     Copyright (c) 2008-2017 Juan Romero Pardines and contributors

require freedesktop-desktop

if ever is_scm; then
    require scm-git
    require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]
fi

export_exlib_phases src_unpack src_prepare src_test

SUMMARY="A tiling window manager, completely written from scratch"
DESCRIPTION="
i3 was created because wmii, our favorite window manager at the time, didn’t provide some features
we wanted (multi-monitor done right, for example), had some bugs, didn’t progress since quite
some time and wasn’t easy to hack at all (source code comments/documentation completely lacking).
Still, we think the wmii developers and contributors did a great job. Thank you for inspiring us
to create i3.

Please be aware that i3 is primarily targeted at advanced users and developers.
"
HOMEPAGE="http://i3wm.org/"

LICENCES="BSD-3"
SLOT="0"

MYOPTIONS="
    ( libc: musl )
"

DEPENDENCIES="
    build:
        app-doc/asciidoc[>=8.3.0]
        app-text/xmlto
        dev-lang/perl:*
        virtual/pkg-config
        x11-proto/xcb-proto[>=1.3]
    build+run:
        dev-libs/glib:2 [[ note = [ libi3/string.c ] ]]
        dev-libs/libev
        dev-libs/pcre[>=8.10]
        dev-libs/yajl[>=2.0.1]
        x11-libs/cairo[>=1.14.4][X]
        x11-libs/libxcb[>=1.1.93]
        x11-libs/libxkbcommon[X][>=0.4.0]
        x11-libs/pango
        x11-libs/startup-notification
        x11-utils/xcb-util
        x11-utils/xcb-util-cursor
        x11-utils/xcb-util-keysyms
        x11-utils/xcb-util-wm
        x11-utils/xcb-util-xrm
        libc:musl? (
            dev-libs/libglob [[
                note = [ use the OpenBSD implementation of GLOB_TILDE ]
            ]]
        )
    test:
        dev-perl/AnyEvent
        dev-perl/AnyEvent-I3
        dev-perl/JSON-XS
        dev-perl/X11-XCB
        x11-server/xorg-server[xephyr]
    recommendation:
        x11-plugins/i3status [[
            description = [ Default source for i3bar ]
        ]]
    suggestion:
        x11-misc/dmenu [[
            description = [ Default program launcher ]
        ]]
        (
            dev-perl/AnyEvent-I3
            dev-perl/JSON-XS
        ) [[
            *description = [ Used by the tree saving utility ]
            *group-name = [ i3-save-tree ]
        ]]
"

ECONF_SOURCE="${WORK}"
WORK="${WORK}"/build

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-debug=yes )

# The tests just hang.
RESTRICT="test"

i3_src_unpack() {
    if ever is_scm; then
        scm_src_unpack
    else
        default
    fi
    edo mkdir -p "${WORK}"
}

i3_src_prepare() {
    if ever is_scm; then
        edo pushd "${ECONF_SOURCE}"
        eautoreconf
        edo popd
    fi

    default

    # Replace GLOB_TILDE and add libglob.a to the libraries
    if [[ $(exhost --target) == *-musl* ]]; then
        edo echo "AM_LDFLAGS = /usr/$(exhost --target)/lib/libglob.so" | edo tee -a ${ECONF_SOURCE}/Makefile.am
        edo sed -i '/include/s,<glob.h>,"libglob/glob.h",' ${ECONF_SOURCE}/i3bar/src/main.c ${ECONF_SOURCE}/libi3/resolve_tilde.c
        edo sed -i '/GLOB_TILDE/s/glob(/g_glob(/g' ${ECONF_SOURCE}/i3bar/src/main.c ${ECONF_SOURCE}/libi3/resolve_tilde.c
        edo sed -i 's/globfree(/g_globfree(/g' ${ECONF_SOURCE}/i3bar/src/main.c ${ECONF_SOURCE}/libi3/resolve_tilde.c
        eautoreconf
    fi
}

i3_src_test() {
    esandbox allow_net --connect "unix-abstract:/tmp/.X11-unix/X0"
    default
    esandbox disallow_net --connect "unix-abstract:/tmp/.X11-unix/X0"
}
