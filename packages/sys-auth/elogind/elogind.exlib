# Copyright 2018 Bjorn Pagen <bjornpagen@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part apon 'elogind-235.2-r2.ebuild' from Gentoo, which is:
#     Copyright 1999-2018 Gentoo Foundation.

SCM_pwx_REPOSITORY="https://github.com/Yamakuzure/pwx-elogind-migration-tools.git"
SCM_SECONDARY_REPOSITORIES="pwx"
SCM_EXTERNAL_REFS="pwx:pwx"

require github [ tag=v${PV} ]
require meson
require openrc-service [ openrc_confd_files=[ "${FILES}"/openrc/confd ] ]

export_exlib_phases src_configure src_test src_install

SUMMARY="The systemd project's logind, extracted to a standalone package"

LICENCES="CC0 LGPL-2.1 public-domain"
SLOT="0"
MYOPTIONS="
    acl doc pam polkit
    ( providers: openrc ) [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build+run:
        sys-apps/eudev
        sys-apps/util-linux
        sys-libs/libcap
        acl? ( sys-apps/acl )
        pam? ( sys-libs/pam )
        polkit? ( sys-auth/polkit:1[>=0.106] )
    build:
        app-text/docbook-xml-dtd:4.2
        app-text/docbook-xml-dtd:4.5
        dev-util/gperf
        dev-util/intltool
        sys-devel/libtool
        virtual/pkg-config
    run:
        !sys-apps/systemd [[
            description = [ sys-apps/systemd and this package both provide logind ]
            resolution = manual
        ]]
    post:
        sys-apps/dbus
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dlocalstatedir=/var
    -Dsplit-usr=false
    -Drootlibdir=/usr/$(exhost --target)/lib
    -Drootprefix=/usr/$(exhost --target)
    -Dsmack=false
    -Dman=true
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES+=(
    acl
    'doc html'
    pam
    polkit
)

elogind_src_configure() {
    if optionq providers:openrc; then
        local rccgroupmode="$(grep rc_cgroup_mode /etc/rc.conf | cut -d '"' -f 2)"
        local cgroupmode="legacy"

        if [[ "xhybrid" = "x${rccgroupmode}" ]] ; then
            cgroupmode="hybrid"
        elif [[ "xunified" = "x${rccgroupmode}" ]] ; then
            cgroupmode="unified"
        fi

        MESON_SRC_CONFIGURE_PARAMS+=(
            -Dcgroup-controller=openrc
            -Ddefault-hierarchy=${cgroupmode}
        )
    fi
    meson_src_configure
}

elogind_src_test() {
    if [[ -f /etc/machine-id ]]; then
        meson_src_test
    else
        ewarn "The tests require a valid, initialised /etc/machine-id which you don't seem to"
        ewarn "have. Please run regenerate your machine-id if you want to run the tests. This"
        ewarn "can be done by running \"dbus-uuidgen --ensure=/etc/machine-id\"."
    fi
}

elogind_src_install() {
    keepdir /var/lib/${PN}
    meson_src_install
    install_openrc_files
}

