# Copyright © 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings
require vala [ vala_dep=true ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="People aggregation library"
DESCRIPTION="
libfolks is a library that aggregates people from multiple sources (eg,
Telepathy connection managers) to create metacontacts.
"
HOMEPAGE="http://telepathy.freedesktop.org/wiki/Folks"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    bluetooth [[ description = [ Pull contacts from bluetooth devices ] ]]
    eds [[ description = [ Enable the evolution-data-server backend ] ]]
    import-tool [[ description = [ Enable the meta-contact import tool ] ]]
    inspect-tool [[
        description = [ Enable the data inspection tool ]
        requires = vapi
    ]]
    telepathy [[ description = [ Telepathy IM provider ] ]]
    tracker [[ description = [ Enable the tracker backend ] ]]
    vapi
    zeitgeist [[ description = [ enable the zeitgeist (telepathy) backend ] requires = telepathy ]]
    ( linguas: ar as be bg bn_IN ca ca@valencia cs da de el en_GB eo es eu fa fi fr gl gu he hi hu
               id it ja kn ko lt lv ml mr nb or pa pl pt pt_BR ru sk sl sr sr@latin sv ta te tg tr
               ug uk vi zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.50.0]
        virtual/pkg-config[>=0.21]
    build+run:
        base/libgee:0.8[>=0.8.4][gobject-introspection]
        dev-libs/dbus-glib:1
        dev-libs/glib:2[>=2.40.0]
        gnome-desktop/gobject-introspection:1[>=1.30]
        sys-apps/dbus
        bluetooth? ( dev-libs/glib:2[>=2.39.2]
                     gnome-desktop/evolution-data-server:1.2[>=3.13.90] )
        eds? ( gnome-desktop/evolution-data-server:1.2[>=3.13.90][gobject-introspection][vapi]
               dev-libs/libxml2:2.0 )
        import-tool? ( dev-libs/libxml2:2.0 )
        telepathy? ( net-im/telepathy-glib[>=0.19.9][vapi] )
        tracker? ( app-pim/tracker:1.0[>=0.15.2][gobject-introspection] )
        zeitgeist? ( dev-libs/zeitgeist:2.0[>=0.9.14] )
"

AT_M4DIR=( m4 )

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-fatal-warnings --disable-libsocialweb-backend
                               --disable-ofono-backend )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'bluetooth bluez-backend' 'eds eds-backend' 'import-tool'
                                       'inspect-tool' 'telepathy telepathy-backend' 'tracker tracker-backend'
                                       'vapi vala' 'zeitgeist' )

RESTRICT="test" # requires X

src_prepare() {
    autotools_src_prepare
    edo intltoolize --force --automake
}

src_install() {
    default
    nonfatal edo rmdir "${IMAGE}"/usr/bin
}

