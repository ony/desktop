# Copyright 2008, 2009 Mike Kelly
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop pam
require autotools [ supported_autoconf=[ 2.5 ]  supported_automake=[ none ] ]

SUMMARY="Modular X screen saver and locker"
DESCRIPTION="
XScreenSaver is the standard screen saver collection shipped on most
Linux and Unix systems running the X11 Window System.
"
HOMEPAGE="https://www.jwz.org/${PN}"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.gz"

BUGS_TO="pioto@exherbo.org"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/changelog.html"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/faq.html"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk [[ description = [ Build the preferences GUI, xscreensaver-demo and some additional screen savers ] ]]
    opengl
    pam
    setuid [[ description = [ Allow some demos to be installed setuid ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( linguas: da de es et fi fr hu it ja ko nb nl pl pt pt_BR ru sk sv vi wa zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext
        virtual/pkg-config
        x11-proto/xorgproto
    build+run:
        dev-libs/libxml2:2.0
        media-libs/libpng:=
        sys-apps/bc
        x11-apps/xdg-utils
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXft[>=2.1.0]
        x11-libs/libXinerama
        x11-libs/libXmu
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXt
        x11-libs/libXxf86misc
        x11-libs/libXxf86vm
        gtk? (
            dev-libs/glib:2
            gnome-platform/libglade:2
            x11-libs/gdk-pixbuf:2.0[X]
            x11-libs/gtk+:2[>=2.22]
        )
        opengl? ( x11-dri/mesa )
        pam? ( sys-libs/pam )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    recommendation:
        x11-apps/appres [[ description = [ needed for xscreensaver-text ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/fix_configure.patch )

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    gtk
    'gtk pixbuf'
    'gtk record-animation'
    'opengl gl'
    pam
    'setuid setuid-hacks'
)
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-locking
    --enable-nls
    --disable-root-passwd
    --with-dpms-ext
    --with-browser=xdg-open
    --with-jpeg
    --with-png
    --with-proc-interrupts
    --with-pthread
    --with-randr-ext
    --with-x-app-defaults=/usr/share/X11/app-defaults
    --with-xdbe-ext
    --with-xf86gamma-ext
    --with-xf86vmode-ext
    --with-xft
    --with-xinerama-ext
    --with-xinput-ext
    --with-xshm-ext
    --without-gle
    --without-gles
    --without-kerberos
    --without-readdisplay
    --without-motif
)

src_prepare() {
    default

    # Makefile.am exists, so aclocal does not fail. Automake won't succeed, so no point in running aclocal
    eautoconf
}

src_install() {
    emake install_prefix="${IMAGE}" install

    if option pam ; then
        pamd_mimic_system "${PN}" auth auth
    fi
}

pkg_postinst() {
    option gtk && freedesktop-desktop_pkg_postinst
}

pkg_postrm() {
    option gtk && freedesktop-desktop_pkg_postrm
}

